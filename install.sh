#!/bin/bash
#check root
[ $(id -u) != "0" ] && { echo "错误: 您必须以root用户运行此脚本"; exit 1; }
rm -rf node*
#常规变量设置
#fonts color
Green="\033[32m" 
Red="\033[31m" 
Yellow="\033[33m"
GreenBG="\033[42;37m"
RedBG="\033[41;37m"
Font="\033[0m"

#notification information
Info="${Green}[Info]${Font}"
OK="${Green}[OK]${Font}"
Error="${Red}[Error]${Font}"
Notification="${Yellow}[Notification]${Font}"

#IP and config
#IPAddress=`wget http://members.3322.org/dyndns/getip -O - -q ; echo`;
config="/root/shadowsocks/userapiconfig.py"
get_ip() {
	ip=$(curl -s https://ipinfo.io/ip)
	[[ -z $ip ]] && ip=$(curl -s https://api.ip.sb/ip)
	[[ -z $ip ]] && ip=$(curl -s https://api.ipify.org)
	[[ -z $ip ]] && ip=$(curl -s https://ip.seeip.org)
	[[ -z $ip ]] && ip=$(curl -s https://ifconfig.co/ip)
	[[ -z $ip ]] && ip=$(curl -s https://api.myip.com | grep -oE "([0-9]{1,3}\.){3}[0-9]{1,3}")
	[[ -z $ip ]] && ip=$(curl -s icanhazip.com)
	[[ -z $ip ]] && ip=$(curl -s myip.ipip.net | grep -oE "([0-9]{1,3}\.){3}[0-9]{1,3}")
	[[ -z $ip ]] && echo -e "\n 获取不到IP,,！\n" && exit
}

node_install_start(){
	apt update -y
	apt install python-pip git supervisor libssl-dev python-dev libffi-dev libsodium-dev iptables vim -y
	cd /root/shadowsocks
	pip install -r requirements.txt
	cp apiconfig.py userapiconfig.py
	cp config.json user-config.json
}
api(){
    clear
	# 取消文件数量限制
	sed -i '$a * hard nofile 512000\n* soft nofile 512000' /etc/security/limits.conf
	echo -e "如果以下手动配置错误，请在${config}手动编辑修改"
	read -p "请输入你的对接域名或IP(例如:http://www.baidu.com 默认为本机对接): " WEBAPI_URL
	read -p "请输入muKey(在你的配置文件中):" WEBAPI_TOKEN
	read -p "请输入测速周期(回车默认为不测速):" SPEEDTEST
	read -p "请输入你的节点编号(回车默认为节点ID 3):  " NODE_ID
	node_install_start
	cd /root/shadowsocks
	echo -e "modify Config.py...\n"
	get_ip
	WEBAPI_URL=${WEBAPI_URL:-"http://${ip}"}
	sed -i '/WEBAPI_URL/c \WEBAPI_URL = '\'${WEBAPI_URL}\''' ${config}
	#sed -i "s#https://zhaoj.in#${WEBAPI_URL}#" /root/shadowsocks/userapiconfig.py
	WEBAPI_TOKEN=${WEBAPI_TOKEN:-"marisn"}
	sed -i '/WEBAPI_TOKEN/c \WEBAPI_TOKEN = '\'${WEBAPI_TOKEN}\''' ${config}
	#sed -i "s#glzjin#${WEBAPI_TOKEN}#" /root/shadowsocks/userapiconfig.py
	SPEEDTEST=${SPEEDTEST:-"0"}
	sed -i '/SPEED/c \SPEEDTEST = '${SPEEDTEST}'' ${config}
	NODE_ID=${NODE_ID:-"3"}
	sed -i '/NODE_ID/c \NODE_ID = '${NODE_ID}'' ${config}
}
db(){
    clear
	# 取消文件数量限制
	sed -i '$a * hard nofile 512000\n* soft nofile 512000' /etc/security/limits.conf
	echo -e "如果以下手动配置错误，请在${config}手动编辑修改"
	read -p "请输入你的对接数据库IP(例如:127.0.0.1 如果是本机请直接回车): " MYSQL_HOST
	read -p "请输入你的数据库名称(默认sspanel):" MYSQL_DB
	read -p "请输入你的数据库端口(默认3306):" MYSQL_PORT
	read -p "请输入你的数据库用户名(默认root):" MYSQL_USER
	read -p "请输入你的数据库密码(默认root):" MYSQL_PASS
	read -p "请输入你的节点编号(回车默认为节点ID 3):  " NODE_ID
	node_install_start
	cd /root/shadowsocks
	echo -e "modify Config.py...\n"
	get_ip
	sed -i '/API_INTERFACE/c \API_INTERFACE = '\'glzjinmod\''' ${config}
	MYSQL_HOST=${MYSQL_HOST:-"${ip}"}
	sed -i '/MYSQL_HOST/c \MYSQL_HOST = '\'${MYSQL_HOST}\''' ${config}
	MYSQL_DB=${MYSQL_DB:-"sspanel"}
	sed -i '/MYSQL_DB/c \MYSQL_DB = '\'${MYSQL_DB}\''' ${config}
	MYSQL_USER=${MYSQL_USER:-"root"}
	sed -i '/MYSQL_USER/c \MYSQL_USER = '\'${MYSQL_USER}\''' ${config}
	MYSQL_PASS=${MYSQL_PASS:-"root"}
	sed -i '/MYSQL_PASS/c \MYSQL_PASS = '\'${MYSQL_PASS}\''' ${config}
	MYSQL_PORT=${MYSQL_PORT:-"3306"}
	sed -i '/MYSQL_PORT/c \MYSQL_PORT = '${MYSQL_PORT}'' ${config}
	NODE_ID=${NODE_ID:-"3"}
	sed -i '/NODE_ID/c \NODE_ID = '${NODE_ID}'' ${config}
}
clear
echo -e "\033[1;5;31m请选择对接模式：\033[0m"
echo -e "1.API对接模式"
echo -e "2.数据库对接模式"
read -t 30 -p "选择：" NODE_MS
case $NODE_MS in
		1)
			api
			;;
		2)
			db
			;;
		*)
		    echo -e "请选择正确对接模式"
			exit 1
			;;
esac
#haveged性能增强
apt install haveged -y
systemctl disable haveged
systemctl enable haveged
systemctl restart haveged

#优化系统内核参数
echo "fs.file-max = 65535" > /etc/sysctl.conf
echo "net.core.rmem_max = 67108864" >> /etc/sysctl.conf
echo "net.core.wmem_max = 67108864" >> /etc/sysctl.conf
echo "net.core.netdev_max_backlog = 250000" >> /etc/sysctl.conf
echo "net.core.somaxconn = 65535" >> /etc/sysctl.conf
echo "net.ipv4.tcp_syncookies = 1" >> /etc/sysctl.conf
echo "net.ipv4.tcp_tw_reuse = 1" >> /etc/sysctl.conf
echo "net.ipv4.tcp_fin_timeout = 30" >> /etc/sysctl.conf
echo "net.ipv4.tcp_keepalive_time = 1200" >> /etc/sysctl.conf
echo "net.ipv4.ip_local_port_range = 10000 65000" >> /etc/sysctl.conf
echo "net.ipv4.tcp_max_syn_backlog = 8192" >> /etc/sysctl.conf
echo "net.ipv4.tcp_max_tw_buckets = 5000" >> /etc/sysctl.conf
echo "net.ipv4.tcp_fastopen = 3" >> /etc/sysctl.conf
echo "net.ipv4.tcp_rmem = 4096 87380 67108864" >> /etc/sysctl.conf
echo "net.ipv4.tcp_wmem = 4096 65536 67108864" >> /etc/sysctl.conf
echo "net.ipv4.tcp_mtu_probing = 1" >> /etc/sysctl.conf
echo "net.core.default_qdisc = fq" >> /etc/sysctl.conf
echo "net.ipv4.tcp_congestion_control = bbr" >> /etc/sysctl.conf
echo "* soft nofile 65535" > /etc/security/limits.conf
echo "* hard nofile 65535" >> /etc/security/limits.conf
echo "* soft nproc 65535" >> /etc/security/limits.conf
echo "* hard nproc 65535" >> /etc/security/limits.conf
ulimit -n 65535
echo "sshd: ALL" > /etc/hosts.allow
sysctl -p


#iptables屏蔽
iptables -I OUTPUT -m string --string "youtube.com" --algo bm -j DROP
iptables -I OUTPUT -m string --string "facebook.com" --algo bm -j DROP
iptables -I OUTPUT -m string --string "Twitter.com" --algo bm -j DROP
iptables -I OUTPUT -m string --string "tumblr.com" --algo bm -j DROP
iptables -I OUTPUT -m string --string "instagram.com" --algo bm -j DROP
iptables -I OUTPUT -m string --string "twitch.tv" --algo bm -j DROP
iptables -I OUTPUT -m string --string "googlevideo.com" --algo bm -j DROP
iptables -I OUTPUT -m string --string "ggpht.com.com" --algo bm -j DROP
iptables -I OUTPUT -m string --string "ytimg.com" --algo bm -j DROP
iptables -I OUTPUT -m string --string "pornhub.com" --algo bm -j DROP
#supervisor运行shadowsocks
cp -rf /root/shadowsocks/ssr.conf /etc/supervisor/conf.d/ssr.conf
supervisorctl reload
echo -e "${OK} ${RedBG} 请手动执行 apt install -y iptables-persistent ${Font}"
echo -e "${OK} ${RedBG} 用于保存 IPTABLES 屏蔽 ${Font}"

echo -e "---------------------------------------------------------"

echo -e "${OK} ${GreenBG} 启动后端命令: supervisorctl start ssr ${Font}"
echo -e "${OK} ${GreenBG} 重启后端命令: supervisorctl restart ssr ${Font}"
echo -e "${OK} ${GreenBG} 停止后端命令: supervisorctl stop ssr ${Font}"
echo -e "${OK} ${GreenBG} Iptables保存命令(持久化)：netfilter-persistent save  ${Font}"

echo -e "---------------------------------------------------------"


